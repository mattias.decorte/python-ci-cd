'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
    statistuqes : H/F de la promotion
'''
import argparse
import datetime as dt
from dateutil import relativedelta as rd
# Import des fonctons de l'application
import functions.application_functions as af

# Version du script
VERSION = "1.2"

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--path", type=str)
parser.add_argument("--version", action="store_true", help="Afficher la version du script")
parser.add_argument("--list", action="store_true", help="Afficher la liste des étudiants")

args = parser.parse_args()

if args.version:
    print(f"statistique_promotion.py : {VERSION}")
    exit()

file = args.path

if file is None:
    file = "promotion_B3_B.csv"
today = dt.datetime.now()

# Boucle de lecture ligne à ligne
with open(file, 'r') as file:
    students_list = []  # Pour stocker la liste des étudiants
    nbrf = nbrh = nbro = total_age = 0
    count = 0
    # On saute la premier ligne
    next(file)
    for line in file:
        if line[0] == '#':
            continue
        fields = line.strip().split(";")
        gender = fields[2]
        bdate = fields[4]
        bdate = dt.datetime.strptime(bdate, '%d/%m/%Y')
        ans, mois = af.gdelta_age(today, bdate)
        total_age += ans * 12 + mois  # Ajout pour calculer l'âge moyen
        count += 1
        student_info = f"{fields[1]} - {fields[0]} - {bdate} , âge : {ans} ans et {mois} mois"
        students_list.append(student_info)
        if args.list:
            print(student_info)
        if gender.lower() == 'h':
            nbrh += 1
        elif gender.lower() == 'f':
            nbrf += 1
        else:
            nbro += 1

nbr_tot = nbrf + nbrh + nbro
sep = "=" * 80

# Affichage des statistiques
print(sep)
print(f"Nombre total d'élèves : {nbr_tot}")
print(f"Nombre total de filles : {nbrf} - {af.cpercent(nbrf, nbr_tot, 2)} %")
print(f"Nombre total de garçons : {nbrh} - {af.cpercent(nbrh, nbr_tot, 2)} %")
print(f"Autres : {nbro} - {af.cpercent(nbro, nbr_tot, 2)} %")

# Calcul de l'âge moyen
if count > 0:
    moy_age = total_age / count
    ans_moyen = int(moy_age // 12)
    mois_moyen = int(moy_age % 12)
    print(f"Âge moyen de la promotion : {ans_moyen} ans et {mois_moyen} mois")

# Affichage de la liste des étudiants si l'option --list est spécifiée
if args.list:
    print(sep)
    print("Liste des étudiants :")
    for student_info in students_list:
        print(student_info)

print(sep)
print("Fin du programme...")
